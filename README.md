# Über dieses Repo

Dieses Repo beinhaltet einen einfache "Vorhersagemaschine", die mit der Python-Bibliothek ein einfaches neuronales Netz trainiert, um einen linearen Zusammenhang zu "lernen".

## Weitere Informationen

Das Projekt wurde auf Basisdes Beispiels unter https://colab.research.google.com/github/tensorflow/examples/blob/master/courses/udacity_intro_to_tensorflow_for_deep_learning/l02c01_celsius_to_fahrenheit.ipynb erstellt. Dieses steht unter einer Apache 2.0 Lizenz.

Begleitende Informationen findet man im Imformatik-Wiki: https://info-bw.de/faecher:informatik:oberstufe:machine_learning:einfuehrung:start

## Verwendung

Man kann die Vorhersagemaschine als "Black Box" folgendermaßen testen:

* Repo klonen
* Das Skript mkvenv.sh erstellt unter Linux ein python Virtual-Environment mit den nötigen Abhängigkeiten und aktiviert es.
* Jetzt kann man den Befehl ''vorhersagemaschine.py -i <inputfile> -t <trainingsrunden> -x <vorhersagestelle> -g'' verwenden, um Trainingsdaten einzulesen, das Netz zu trainieren und einen Vorhersagewert für eine Stelle auszugeben,