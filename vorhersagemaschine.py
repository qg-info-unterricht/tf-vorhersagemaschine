#!./venv/bin/python3

# Bibliotheken einbinden
# TF Warnungen wegen Grafikkartenunterstuetung unterdruecken
import os,sys,getopt
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 
import tensorflow as tf
import numpy as np
import logging
import matplotlib.pyplot as plt

logger = tf.get_logger()
logger.setLevel(logging.ERROR)

# Optionen verarbeiten
showgraph = False
opts, args = getopt.getopt(sys.argv[1:],"hi:t:x:g")
for opt, arg in opts:
    if opt == '-h':
        print('vorhersagemaschine.py -i <inputfile> -t <trainingsrunden> -x <vorhersagepunkt>')
        sys.exit()
    elif opt == '-i':
        inputfile = arg
    elif opt == '-t':
        trainingsrunden = int(arg)
    elif opt == '-x':
        xtoguess = float(arg)
    elif opt == '-g':
        showgraph = True


# Daten einlesen
with open(inputfile) as f:
    lines=f.readlines()

x = np.fromstring(lines[0], dtype=float, sep=',')
y = np.fromstring(lines[1], dtype=float, sep=',')

# Kontrollausgabe
print("Das Modell wird mit den folgenden Zuordnungen trainiert:")
for i,c in enumerate(x):
  print("{} -> {}".format(c, y[i]))

###############################################
# Tensorflow Modell
l0 = tf.keras.layers.Dense(units=1, input_shape=[1])
model = tf.keras.Sequential([l0])
model.compile(loss='mean_squared_error',optimizer=tf.keras.optimizers.Adam(0.1))
###############################################

# Training
history = model.fit(x, y, epochs=trainingsrunden, verbose=False)
print("Training beendet.")

print("Vorhersage für x={} ist y={}".format(xtoguess,model.predict([xtoguess])))

print("Die Werte der trainierten Variablen sind: {}".format(l0.get_weights()))

if showgraph:
    plt.xlabel('Trainingsrunde')
    plt.ylabel("Fehler")
    plt.plot(history.history['loss'])
    plt.show()
